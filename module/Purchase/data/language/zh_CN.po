msgid ""
msgstr ""
"Project-Id-Version: DBErp\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-26 18:02+0800\n"
"PO-Revision-Date: 2019-04-26 18:02+0800\n"
"Last-Translator: Baron <baron@loongdom.cn>\n"
"Language-Team: DBErp <baron@loongdom.cn>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: translate\n"
"X-Poedit-Basepath: ../..\n"
"X-Generator: Poedit 2.0.7\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"

#: src/Controller/OrderController.php:122
msgid "采购订单添加成功！"
msgstr ""

#: src/Controller/OrderController.php:123
#: src/Controller/OrderController.php:176
#: src/Controller/OrderController.php:232
#: src/Controller/OrderController.php:269
#: src/Controller/OrderController.php:298
#: src/Controller/OrderController.php:322
msgid "采购订单"
msgstr ""

#: src/Controller/OrderController.php:127
msgid "采购订单添加失败！"
msgstr ""

#: src/Controller/OrderController.php:146
#: src/Controller/OrderController.php:201
#: src/Controller/OrderReturnController.php:205
msgid "该订单不存在！"
msgstr ""

#: src/Controller/OrderController.php:150
msgid "该订单所属状态不能被编辑！"
msgstr ""

#: src/Controller/OrderController.php:175
msgid "采购订单编辑成功！"
msgstr ""

#: src/Controller/OrderController.php:180
msgid "采购订单编辑失败！"
msgstr ""

#: src/Controller/OrderController.php:231
msgid "采购订单删除成功！"
msgstr ""

#: src/Controller/OrderController.php:236
msgid "采购订单删除失败！"
msgstr ""

#: src/Controller/OrderController.php:269
msgid "删除商品"
msgstr ""

#: src/Controller/OrderController.php:292
msgid "审核采购单失败！"
msgstr ""

#: src/Controller/OrderController.php:297
msgid "采购单审核完成！"
msgstr ""

#: src/Controller/OrderController.php:315
msgid "取消审核采购单失败！"
msgstr ""

#: src/Controller/OrderController.php:316
msgid "存在采购退货，无法取消审核采购单！"
msgstr ""

#: src/Controller/OrderController.php:321
msgid "采购单取消审核完成！"
msgstr ""

#: src/Controller/OrderReturnController.php:98
#: src/Controller/OrderReturnController.php:119
#: src/Controller/OrderReturnController.php:147
msgid "该采购退货单不存在！"
msgstr ""

#: src/Controller/OrderReturnController.php:122
msgid "该采购退货不能执行完成操作！"
msgstr ""

#: src/Controller/OrderReturnController.php:128
msgid "退货操作完成！"
msgstr ""

#: src/Controller/OrderReturnController.php:129
#: src/Controller/OrderReturnController.php:184
#: src/Controller/OrderReturnController.php:239
#: view/purchase/order/index.phtml:123
msgid "采购退货"
msgstr ""

#: src/Controller/OrderReturnController.php:150
msgid "该采购退货不能执行取消操作！"
msgstr ""

#: src/Controller/OrderReturnController.php:183
msgid "取消退货成功！"
msgstr ""

#: src/Controller/OrderReturnController.php:187
msgid "取消退货失败！"
msgstr ""

#: src/Controller/OrderReturnController.php:209
msgid "该订单状态不符合退货要求！"
msgstr ""

#: src/Controller/OrderReturnController.php:238
msgid "退货添加完成！"
msgstr ""

#: src/Controller/OrderReturnController.php:246
msgid "退货添加失败！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:97
msgid "该订单状态不符或者订单不存在！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:128
msgid "验货入库成功！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:129
#: src/Controller/WarehouseOrderController.php:197
#: src/Controller/WarehouseOrderController.php:238
msgid "采购入库"
msgstr ""

#: src/Controller/WarehouseOrderController.php:133
msgid "验货入库失败！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:153
msgid "该入库单不存在！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:177
#: src/Controller/WarehouseOrderController.php:223
msgid "该入库单状态不符或者入库单不存在！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:196
msgid "采购入库成功！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:201
msgid "采购入库失败！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:237
msgid "待入库单删除成功！"
msgstr ""

#: src/Controller/WarehouseOrderController.php:242
msgid "待入库单删除失败！"
msgstr ""

#: src/Form/OrderGoodsReturnForm.php:100
#: view/purchase/order-return/return-order.phtml:113
msgid "请选择退货商品"
msgstr ""

#: src/Form/SearchOrderForm.php:45 src/Form/SearchOrderReturnForm.php:45
#: src/Form/SearchWarehouseOrderForm.php:45
msgid "起始金额"
msgstr ""

#: src/Form/SearchOrderForm.php:55 src/Form/SearchOrderReturnForm.php:55
#: src/Form/SearchWarehouseOrderForm.php:55
msgid "结束金额"
msgstr ""

#: src/Form/SearchOrderForm.php:65 src/Form/SearchOrderReturnForm.php:65
#: view/purchase/order-return/index.phtml:23
#: view/purchase/order-return/return-order.phtml:31
#: view/purchase/order-return/view.phtml:24 view/purchase/order/add.phtml:12
#: view/purchase/order/add.phtml:45 view/purchase/order/index.phtml:24
#: view/purchase/order/view.phtml:24 view/purchase/warehouse-order/add.phtml:80
#: view/purchase/warehouse-order/view.phtml:54
msgid "采购单号"
msgstr ""

#: src/Form/SearchOrderForm.php:75 src/Form/SearchOrderReturnForm.php:75
#: src/Form/SearchWarehouseOrderForm.php:75
#: view/purchase/order-return/return-order.phtml:58
#: view/purchase/order-return/view.phtml:45 view/purchase/order/add.phtml:72
#: view/purchase/order/view.phtml:50
#: view/purchase/warehouse-order/add.phtml:100
#: view/purchase/warehouse-order/view.phtml:74
msgid "供应商联系人"
msgstr ""

#: src/Form/SearchOrderForm.php:85 src/Form/SearchOrderReturnForm.php:85
#: src/Form/SearchWarehouseOrderForm.php:85
msgid "供应商电话"
msgstr ""

#: src/Form/SearchWarehouseOrderForm.php:65
#: view/purchase/warehouse-order/add.phtml:9
#: view/purchase/warehouse-order/add.phtml:41
#: view/purchase/warehouse-order/index.phtml:23
#: view/purchase/warehouse-order/view.phtml:23
msgid "入库单号"
msgstr ""

#: view/purchase/order-return/index.phtml:23 view/purchase/order/index.phtml:24
#: view/purchase/warehouse-order/index.phtml:23
msgid "订单数："
msgstr ""

#: view/purchase/order-return/index.phtml:24
#: view/purchase/order-return/return-order.phtml:52
#: view/purchase/order-return/view.phtml:30 view/purchase/order/add.phtml:53
#: view/purchase/order/index.phtml:25 view/purchase/order/view.phtml:45
#: view/purchase/warehouse-order/add.phtml:86
#: view/purchase/warehouse-order/index.phtml:24
#: view/purchase/warehouse-order/view.phtml:60
msgid "供应商"
msgstr ""

#: view/purchase/order-return/index.phtml:25 view/purchase/order/index.phtml:26
#: view/purchase/warehouse-order/index.phtml:25
msgid "联系人"
msgstr ""

#: view/purchase/order-return/index.phtml:26 view/purchase/order/index.phtml:27
#: view/purchase/warehouse-order/index.phtml:26
msgid "联系电话"
msgstr ""

#: view/purchase/order-return/index.phtml:27
#: view/purchase/order-return/return-order.phtml:106
msgid "退货金额"
msgstr ""

#: view/purchase/order-return/index.phtml:28 view/purchase/order/index.phtml:30
#: view/purchase/order/view.phtml:128
#: view/purchase/warehouse-order/add.phtml:57
#: view/purchase/warehouse-order/index.phtml:29
#: view/purchase/warehouse-order/view.phtml:35
msgid "状态"
msgstr ""

#: view/purchase/order-return/index.phtml:29 view/purchase/order/add.phtml:132
#: view/purchase/order/index.phtml:32
#: view/purchase/warehouse-order/index.phtml:30
msgid "操作"
msgstr ""

#: view/purchase/order-return/index.phtml:88
#: view/purchase/order/index.phtml:118
#: view/purchase/warehouse-order/index.phtml:95
msgid "查看"
msgstr ""

#: view/purchase/order-return/index.phtml:91
msgid "退货完成"
msgstr ""

#: view/purchase/order-return/index.phtml:93
msgid "取消退货"
msgstr ""

#: view/purchase/order-return/return-order.phtml:11
#: view/purchase/order/add.phtml:25 view/purchase/order/view.phtml:7
msgid "返回采购订单列表"
msgstr ""

#: view/purchase/order-return/return-order.phtml:12
#: view/purchase/order-return/return-order.phtml:140
msgid "点击退货"
msgstr ""

#: view/purchase/order-return/return-order.phtml:23
msgid "订单信息"
msgstr ""

#: view/purchase/order-return/return-order.phtml:37
msgid "订单金额"
msgstr ""

#: view/purchase/order-return/return-order.phtml:43
#: view/purchase/order/add.phtml:61 view/purchase/order/index.phtml:28
#: view/purchase/order/view.phtml:36 view/purchase/warehouse-order/add.phtml:92
#: view/purchase/warehouse-order/index.phtml:27
#: view/purchase/warehouse-order/view.phtml:66
msgid "付款方式"
msgstr ""

#: view/purchase/order-return/return-order.phtml:64
#: view/purchase/order-return/view.phtml:51 view/purchase/order/add.phtml:80
#: view/purchase/order/view.phtml:56
#: view/purchase/warehouse-order/add.phtml:106
#: view/purchase/warehouse-order/view.phtml:80
msgid "供应商手机"
msgstr ""

#: view/purchase/order-return/return-order.phtml:73
#: view/purchase/order-return/view.phtml:57 view/purchase/order/add.phtml:88
#: view/purchase/order/view.phtml:65
#: view/purchase/warehouse-order/add.phtml:112
#: view/purchase/warehouse-order/view.phtml:86
msgid "供应商座机"
msgstr ""

#: view/purchase/order-return/return-order.phtml:78
#: view/purchase/order-return/view.phtml:66 view/purchase/order/add.phtml:16
#: view/purchase/order/add.phtml:99 view/purchase/order/view.phtml:70
#: view/purchase/warehouse-order/add.phtml:120
#: view/purchase/warehouse-order/view.phtml:94
msgid "备注"
msgstr ""

#: view/purchase/order-return/return-order.phtml:85
msgid "选择退货商品"
msgstr ""

#: view/purchase/order-return/return-order.phtml:97
msgid "选择"
msgstr ""

#: view/purchase/order-return/return-order.phtml:98
#: view/purchase/order-return/view.phtml:80 view/purchase/order/add.phtml:124
#: view/purchase/order/view.phtml:84
#: view/purchase/warehouse-order/add.phtml:132
#: view/purchase/warehouse-order/view.phtml:106
msgid "商品编号"
msgstr ""

#: view/purchase/order-return/return-order.phtml:99
#: view/purchase/order-return/view.phtml:81 view/purchase/order/add.phtml:125
#: view/purchase/order/view.phtml:85
#: view/purchase/warehouse-order/add.phtml:133
#: view/purchase/warehouse-order/view.phtml:107
msgid "商品名称"
msgstr ""

#: view/purchase/order-return/return-order.phtml:100
#: view/purchase/order-return/view.phtml:82 view/purchase/order/add.phtml:126
#: view/purchase/order/view.phtml:86
#: view/purchase/warehouse-order/add.phtml:134
#: view/purchase/warehouse-order/view.phtml:108
msgid "商品规格"
msgstr ""

#: view/purchase/order-return/return-order.phtml:101
#: view/purchase/order-return/view.phtml:84 view/purchase/order/add.phtml:128
#: view/purchase/order/view.phtml:88
#: view/purchase/warehouse-order/add.phtml:135
#: view/purchase/warehouse-order/view.phtml:110
msgid "采购单价"
msgstr ""

#: view/purchase/order-return/return-order.phtml:102
#: view/purchase/order-return/view.phtml:85 view/purchase/order/add.phtml:129
#: view/purchase/order/view.phtml:89
#: view/purchase/warehouse-order/add.phtml:136
#: view/purchase/warehouse-order/view.phtml:111
msgid "采购数量"
msgstr ""

#: view/purchase/order-return/return-order.phtml:103
#: view/purchase/order/add.phtml:130 view/purchase/order/view.phtml:90
msgid "税金"
msgstr ""

#: view/purchase/order-return/return-order.phtml:104
#: view/purchase/order/add.phtml:131 view/purchase/order/view.phtml:91
#: view/purchase/warehouse-order/add.phtml:137
#: view/purchase/warehouse-order/view.phtml:112
msgid "采购总价"
msgstr ""

#: view/purchase/order-return/return-order.phtml:105
msgid "退货数量"
msgstr ""

#: view/purchase/order-return/return-order.phtml:130
msgid "退货原因"
msgstr ""

#: view/purchase/order-return/view.phtml:7
msgid "返回退货列表"
msgstr ""

#: view/purchase/order-return/view.phtml:17 view/purchase/order/view.phtml:17
msgid "查看采购订单"
msgstr ""

#: view/purchase/order-return/view.phtml:36
msgid "退货状态"
msgstr ""

#: view/purchase/order-return/view.phtml:74
msgid "退货商品"
msgstr ""

#: view/purchase/order-return/view.phtml:83 view/purchase/order/add.phtml:127
#: view/purchase/order/view.phtml:87
#: view/purchase/warehouse-order/view.phtml:109
msgid "单位"
msgstr ""

#: view/purchase/order-return/view.phtml:86
msgid "商品退货总价"
msgstr ""

#: view/purchase/order-return/view.phtml:107
msgid "退货金额合计"
msgstr ""

#: view/purchase/order/add.phtml:26 view/purchase/order/add.phtml:170
msgid "保存采购订单"
msgstr ""

#: view/purchase/order/add.phtml:37
msgid "编辑采购订单"
msgstr ""

#: view/purchase/order/add.phtml:37 view/purchase/order/index.phtml:7
msgid "添加采购订单"
msgstr ""

#: view/purchase/order/add.phtml:111 view/purchase/order/view.phtml:78
#: view/purchase/warehouse-order/add.phtml:126
#: view/purchase/warehouse-order/view.phtml:100
msgid "采购商品"
msgstr ""

#: view/purchase/order/add.phtml:113
msgid "点击添加商品"
msgstr ""

#: view/purchase/order/add.phtml:153 view/purchase/order/add.phtml:272
#: view/purchase/order/index.phtml:112
#: view/purchase/warehouse-order/index.phtml:100
msgid "删除"
msgstr ""

#: view/purchase/order/add.phtml:162 view/purchase/order/view.phtml:115
#: view/purchase/warehouse-order/view.phtml:144
msgid "采购金额合计"
msgstr ""

#: view/purchase/order/add.phtml:208
msgid "采购单号不能为空！"
msgstr ""

#: view/purchase/order/add.phtml:211 view/purchase/order/add.phtml:212
msgid "请选择供应商！"
msgstr ""

#: view/purchase/order/add.phtml:215
msgid "请选择付款方式！"
msgstr ""

#: view/purchase/order/add.phtml:218
msgid "请输入联系人！"
msgstr ""

#: view/purchase/order/add.phtml:223
msgid "请添加采购商品！"
msgstr ""

#: view/purchase/order/add.phtml:247
msgid "请填写需要添加的商品名称！"
msgstr ""

#: view/purchase/order/add.phtml:256
msgid "该商品已经添加！"
msgstr ""

#: view/purchase/order/add.phtml:310
msgid "商品删除失败！"
msgstr ""

#: view/purchase/order/index.phtml:29
#: view/purchase/warehouse-order/index.phtml:28
msgid "采购金额"
msgstr ""

#: view/purchase/order/index.phtml:31
msgid "退货"
msgstr ""

#: view/purchase/order/index.phtml:103
msgid "有"
msgstr ""

#: view/purchase/order/index.phtml:103
msgid "无"
msgstr ""

#: view/purchase/order/index.phtml:108
msgid "编辑"
msgstr ""

#: view/purchase/order/index.phtml:110
msgid "审核"
msgstr ""

#: view/purchase/order/index.phtml:111
msgid "您确实要删除该采购单吗？"
msgstr ""

#: view/purchase/order/index.phtml:125
#: view/purchase/warehouse-order/add.phtml:22
#: view/purchase/warehouse-order/add.phtml:156
msgid "验货入库"
msgstr ""

#: view/purchase/order/index.phtml:126
msgid "取消审核"
msgstr ""

#: view/purchase/order/view.phtml:30
msgid "订单状态"
msgstr ""

#: view/purchase/order/view.phtml:122
msgid "操作记录"
msgstr ""

#: view/purchase/order/view.phtml:129
msgid "操作人"
msgstr ""

#: view/purchase/order/view.phtml:130
msgid "操作时间"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:13
#: view/purchase/warehouse-order/add.phtml:68
#: view/purchase/warehouse-order/view.phtml:44
msgid "入库备注"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:21
msgid "返回采购单列表"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:33
msgid "验货入库处理"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:49
#: view/purchase/warehouse-order/view.phtml:29
msgid "仓库"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:185
msgid "入库单号不能为空！"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:188
#: view/purchase/warehouse-order/add.phtml:189
msgid "请选择仓库！"
msgstr ""

#: view/purchase/warehouse-order/add.phtml:192
msgid "请选择状态！"
msgstr ""

#: view/purchase/warehouse-order/index.phtml:99
msgid "入库"
msgstr ""

#: view/purchase/warehouse-order/index.phtml:100
msgid "您确实要删除该待入库单吗？"
msgstr ""

#: view/purchase/warehouse-order/view.phtml:6
msgid "返回采购入库单列表"
msgstr ""

#: view/purchase/warehouse-order/view.phtml:16
msgid "入库单"
msgstr ""
